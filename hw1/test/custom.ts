import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("test of or with three possibilities", () => {

        // put the body of the test here
        const e = new EngExp().match("abc").or("def").or("123").asRegExp();

        expect(e.test("abc")).to.be.true;
        expect(e.test("def")).to.be.true;
        expect(e.test("123")).to.be.true;
        expect(e.test("a")).to.be.false;
        expect(e.test("")).to.be.false;
    });

    // You should modify the above test so it's interesting, and provide
    // at least 4 more nontrivial tests.
    it("or should work correctly with then and other operators", () => {
        const e1 = new EngExp().match("abc").or("def").then("123").asRegExp();
        const e2 = new EngExp().match("abc").then("def").or("123").asRegExp();
        const e3 = new EngExp().match("abc")
            .then(new EngExp().match("def").or("123"))
            .asRegExp();

        const e4 = new EngExp()
            .match("abc")
            .or("123")
            .optional()
            .then("d")
            .asRegExp();
        const e5 = new EngExp()
            .match("abc")
            .oneOrMore(new EngExp().match("d").or("e"))
            .asRegExp();
        const e6 = new EngExp()
            .maybe(new EngExp().match("d").or("e"))
            .then("a")
            .asRegExp();
        const e7 = new EngExp()
            .match("a")
            .then(new EngExp().match("b").or("c"))
            .asRegExp();

        expect(e1.test("abc123")).to.be.true;
        expect(e1.test("def123")).to.be.true;
        expect(e1.test("123")).to.be.false;

        expect(e2.test("abcdef")).to.be.true;
        expect(e2.test("123")).to.be.true;
        expect(e2.test("abc")).to.be.false;
        expect(e2.test("def")).to.be.false;

        expect(e3.test("abcdef")).to.be.true;
        expect(e3.test("abc123")).to.be.true;
        expect(e3.test("abc")).to.be.false;
        expect(e3.test("123")).to.be.false;

        expect(e4.test("d")).to.be.true;
        expect(e4.test("abcd")).to.be.true;
        expect(e4.test("123d")).to.be.true;
        expect(e4.test("abc")).to.be.false;

        expect(e5.test("abcd")).to.be.true;
        expect(e5.test("abcde")).to.be.true;
        expect(e5.test("d")).to.be.false;
        expect(e5.test("e")).to.be.false;
        expect(e5.test("abc")).to.be.false;

        expect(e6.test("a")).to.be.true;
        expect(e6.test("da")).to.be.true;
        expect(e6.test("ea")).to.be.true;
        expect(e6.test("d")).to.be.false;
        expect(e6.test("e")).to.be.false;

        expect(e7.test("ab")).to.be.true;
        expect(e7.test("ac")).to.be.true;
        expect(e7.test("c")).to.be.false;
        expect(e7.test("a")).to.be.false;
        expect(e7.test("b")).to.be.false;
    });

    it("beginLevel should create a separate nesting level", () => {
        const e1 = new EngExp()
            .match("a")
            .beginLevel()
              .match("b")
              .optional()
            .endLevel()
            .then("c")
            .asRegExp();

        const e2 = new EngExp()
            .match("a")
            .beginLevel()
              .match("b")
              .or("c")
            .endLevel()
            .asRegExp();

        const e3 = new EngExp()
            .beginLevel()
              .match("a")
              .or("b")
            .endLevel()
            .beginLevel()
              .match("c")
              .or("d")
            .endLevel()
            .asRegExp();

        expect(e1.test("ac")).to.be.true;
        expect(e1.test("abc")).to.be.true;
        expect(e1.test("a")).to.be.false;
        expect(e1.test("c")).to.be.false;

        expect(e3.test("ac")).to.be.true;
        expect(e3.test("ad")).to.be.true;
        expect(e3.test("bc")).to.be.true;
        expect(e3.test("bd")).to.be.true;
        expect(e3.test("a")).to.be.false;
        expect(e3.test("a")).to.be.false;
        expect(e3.test("c")).to.be.false;
        expect(e3.test("d")).to.be.false;
    });

    it("beginCapture should create create a separate nesting level", () => {
        const e1 = new EngExp()
            .match("a")
            .beginCapture()
              .match("b")
              .optional()
            .endCapture()
            .then("c")
            .asRegExp();

        const e2 = new EngExp()
            .match("a")
            .beginCapture()
              .match("b")
              .or("c")
            .endCapture()
            .asRegExp();

        const e3 = new EngExp()
            .beginCapture()
              .match("a")
              .or("b")
            .endCapture()
            .beginCapture()
              .match("c")
              .or("d")
            .endCapture()
            .asRegExp();

        const e4 = new EngExp()
            .match("a")
            .beginLevel()
              .match("b")
              .beginLevel()
                .match("c")
                .or("d")
              .endLevel()
              .then("e")
              .optional()
            .endLevel()
            .then("f")
            .asRegExp();

        expect(e1.test("ac")).to.be.true;
        expect(e1.test("abc")).to.be.true;
        expect(e1.test("a")).to.be.false;
        expect(e1.test("c")).to.be.false;

        expect(e3.test("ac")).to.be.true;
        expect(e3.test("ad")).to.be.true;
        expect(e3.test("bc")).to.be.true;
        expect(e3.test("bd")).to.be.true;
        expect(e3.test("a")).to.be.false;
        expect(e3.test("a")).to.be.false;
        expect(e3.test("c")).to.be.false;
        expect(e3.test("d")).to.be.false;

        expect(e4.test("af")).to.be.true;
        expect(e4.test("abcef")).to.be.true;
        expect(e4.test("abdef")).to.be.true;
        expect(e4.test("acef")).to.be.false;
        expect(e4.test("adef")).to.be.false;
        expect(e4.test("abef")).to.be.false;
        expect(e4.test("adef")).to.be.false;
        expect(e4.test("abcf")).to.be.false;
        expect(e4.test("abdf")).to.be.false;
        expect(e4.test("abf")).to.be.false;
    });

    it("should throw an error given unmatched nesting levels", () => {
        function f1() {
            return new EngExp().beginLevel().asRegExp();
        }

        function f2() {
            return new EngExp().beginLevel().endLevel().beginLevel().asRegExp();
        }

        function f3() {
            return new EngExp()
                .beginLevel()
                .beginLevel()
                .endLevel()
                .asRegExp();
        }

        function f4() {
            return new EngExp().match("a").beginLevel().match("b").asRegExp();
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
        expect(f3).to.throw(EngExpError);
        expect(f4).to.throw(EngExpError);
    });

    it("should throw an error when endLevel is used without beginLevel", () => {
        function f1() {
            return new EngExp().endLevel().asRegExp();
        }

        function f2() {
            return new EngExp()
                .beginLevel()
                .endLevel()
                .endLevel()
                .asRegExp();
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
    });

    it("should throw an error given unmatched capture groups", () => {
        function f1() {
            return new EngExp().beginCapture().asRegExp();
        }

        function f2() {
            return new EngExp().beginCapture().endCapture().beginCapture().asRegExp();
        }

        function f3() {
            return new EngExp()
                .beginCapture()
                .beginCapture()
                .endCapture()
                .asRegExp();
        }

        function f4() {
            return new EngExp().match("a").beginCapture().match("b").asRegExp();
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
        expect(f3).to.throw(EngExpError);
        expect(f4).to.throw(EngExpError);
    });

    it("should throw an error when endCapture is used without beginCapture", () => {
        function f1() {
            return new EngExp().endCapture().asRegExp();
        }

        function f2() {
            return new EngExp()
                .beginCapture()
                .endCapture()
                .endCapture()
                .asRegExp();
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
    });

    it("should throw an error when closing level with wrong type", () => {
        function f1() {
            return new EngExp().beginLevel().endCapture().asRegExp();
        }

        function f2() {
            return new EngExp().beginCapture().endLevel().asRegExp();
        }

        function f3() {
            return new EngExp()
                .beginLevel()
                .beginCapture()
                .endLevel()
                .endCapture()
                .asRegExp();
        }

        function f4() {
            return new EngExp()
                .beginCapture()
                .beginLevel()
                .endCapture()
                .endLevel()
                .asRegExp();
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
        expect(f3).to.throw(EngExpError);
        expect(f4).to.throw(EngExpError);
    });

    it("should throw on duplicate capture group names", () => {
        function f1() {
            return new EngExp()
                .beginCapture("a")
                .endCapture("a")
                .beginCapture("a")
                .endCapture("a")
                .asRegExp();
        }

        function f2() {
            return new EngExp()
                .beginCapture("a")
                .beginCapture("a")
                .endCapture("a")
                .endCapture("a")
                .asRegExp();
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
    });

    it("should throw when closing with wrong name", () => {
        function f1() {
            return new EngExp()
                .beginCapture("a")
                .endCapture("b")
                .asRegExp();
        }

        expect(f1).to.throw(EngExpError);
    });

    it("should not throw when using or and find correct capture", () => {
        const e1 = new EngExp()
            .beginCapture("a")
            .match("a")
            .endCapture("a")
            .or(new EngExp().beginCapture("a").match("b").endCapture("a"))
            .asRegExp();

        const r1 = e1.exec("a");
        const r2 = e1.exec("b");
        expect(r1.groups.get("a")).to.equal("a");
        expect(r2.groups.get("a")).to.equal("b");
    });
});
