export class EngExpError extends Error {}

class NamedRegExpExecArray extends Array<string> {
    groups: Map<string, string | undefined> = new Map<string, string | undefined>();
}

class NamedRegExp extends RegExp {
    private groups = new Map<string, number[]>();

    constructor(pattern: string,
                flags: string,
                groups: Map<string, number[]>) {
        super(pattern, flags);
        this.groups = new Map<string, number[]>(groups);
    }

    public exec(s: string): NamedRegExpExecArray | null {
        const matchResult = super.exec(s)
        if (matchResult == null) {
            return null;
        }

        const result = new NamedRegExpExecArray();
        for (const elt of matchResult) {
            result.push(elt);
        }

        for (const [name, indices] of this.groups) {
            result.groups.set(name, undefined);
            for (const index of indices) {
                if (matchResult[index] != undefined) {
                    result.groups.set(name, matchResult[index]);
                    break;
                }
            }
        }

        return result;
    }
}

export class EngExp {
    private groups: Map<string, number[]> = new Map<string, number[]>();

    private flags: string = "m";
    private pattern: string = "";
    // You can add new fields here
    // Whenever a new nesting level is started, all text for the current level
    // is pushed onto this stack. The first element of the tuple is the text
    // itself, and the second level is whether or not it's a caputer group. The
    // second part indices if this is a capture group. If this is true, then the
    // third part gives the name of the corresponding capture group.
    private parentPatterns: [string, boolean, string | null][] = [];

    // Keeps track of which capture group we're currently on.
    private currentCapture: number = 1;


    // Don't change sanitize()
    private static sanitize(s: string | EngExp): string | EngExp {
        if (s instanceof EngExp)
            return s;
        else {
            return s.replace(/[^A-Za-z0-9_]/g, "\\$&");
        }
    }

    // Don't change the following three public methods either
    public valueOf(): string {
        return this.asRegExp().source;
    }

    public toString(): string {
        return this.asRegExp().source;
    }

    public withFlags(flags: string) {
        if (/[^gimuy]/g.test(flags))
            throw new EngExpError("invalid flags");
        this.flags = "".concat(...new Set(flags));
        return this;
    }
    // End methods you shouldn't touch

    // asRegExp() will always be called before using your EngExp as a RegExp,
    // or before converting it into a string (for example to include via ``
    // template literals in another EngExp). You might want to take advantage
    // of this for debugging or error checking.
    public asRegExp(): NamedRegExp {
        if (this.parentPatterns.length > 0) {
            throw new EngExpError("unclosed nesting level");
        }

        return new NamedRegExp(this.pattern, this.flags, this.groups);
    }

    // There is a bug somewhere in this code... you'll need to find it

    public match(literal: string): EngExp {
        return this.then(literal);
    }

    public then(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})`;
        if (pattern instanceof EngExp) {
            this.appendCaptures(pattern);
        }

        return this;
    }

    public startOfLine(): EngExp {
        this.pattern += "^";
        return this;
    }

    public endOfLine(): EngExp {
        this.pattern += "$";
        return this;
    }

    public zeroOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.zeroOrMore());
        else {
            this.pattern = `(?:${this.pattern})*`;
            return this;
        }
    }

    public oneOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.oneOrMore());
        else {
            this.pattern = `(?:${this.pattern})+`;
            return this;
        }
    }

    public optional(): EngExp {
        this.pattern = `(?:${this.pattern})?`;
        return this;
    }

    public maybe(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})?`;
        if (pattern instanceof EngExp) {
            this.appendCaptures(pattern);
        }

        return this;
    }

    public anythingBut(characters: string): EngExp {
        this.pattern += `[^${EngExp.sanitize(characters)}]*`;
        return this;
    }

    public digit(): EngExp {
        this.pattern += "\\d";
        return this;
    }

    public repeated(min: number, max?: number): EngExp {
        if (max === undefined) {
            this.pattern = `(?:${this.pattern}){${min}}`;
        }
        else {
            this.pattern = `(?:${this.pattern}){${min},${max}}`;
        }
        return this;
    }

    public multiple(pattern: string | EngExp, min: number, max?: number) {
        if (max === undefined) {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${min}}`;
        }
        else {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${min},${max}}`;
        }

        if (pattern instanceof EngExp) {
            this.appendCaptures(pattern);
        }

        return this;
    }

    // You need to implement these five operators:

    public or(pattern: string | EngExp): EngExp {
        this.pattern =
            `(?:(?:${this.pattern})|(?:${EngExp.sanitize(pattern)}))`;
        if (pattern instanceof EngExp) {
            this.appendCaptures(pattern, true);
        }

        return this;
    }

    public beginCapture(name?: string): EngExp {
        this.parentPatterns.push([this.pattern, true, name]);
        this.pattern = "";

        if (name != null) {
            if (this.groups.has(name)) {
                throw new EngExpError("duplicate named capture");
            }

            this.groups.set(name, [this.currentCapture]);
            this.currentCapture++;
        }

        return this;
    }

    public endCapture(name?: string): EngExp {
        if (this.parentPatterns.length === 0) {
            throw new EngExpError("unmatched endCapture");
        }

        let [parentPattern, isCapture, captureName] = this.parentPatterns.pop();
        if (!isCapture) {
            throw new EngExpError("expected endLevel but found endCapture");
        }

        if (name != null) {
            if (!this.groups.has(name)) {
                throw new EngExpError("unknown capture name");
            }

            if (name !== captureName) {
                throw new EngExpError("capture group name mismatch");
            }
        }

        this.pattern = parentPattern + `(${this.pattern})`;
        return this;
    }

    public beginLevel(): EngExp {
        this.parentPatterns.push([this.pattern, false, null]);
        this.pattern = "";
        return this;
    }

    public endLevel(): EngExp {
        if (this.parentPatterns.length === 0) {
            throw new EngExpError("unmatched endLevel");
        }

        let [parentPattern, isCapture, captureName] = this.parentPatterns.pop();
        if (isCapture) {
            throw new EngExpError("expected endCapture but found endLevel");
        }

        this.pattern = parentPattern + `(?:${this.pattern})`;
        return this;
    }

    // Takes all captures in other and adds them as captures to this EngExp. If
    // isOr is true, then conflicting names are allowed.
    private appendCaptures(other: EngExp, isOr = false) {
        for (const [name, indices] of other.groups) {
            if (this.groups.has(name)) {
                if (!isOr) {
                    throw new EngExpError("duplicate named capture");
                }

                for (const index of indices) {
                    this.groups.get(name).push(index + this.currentCapture - 1);
                }
            } else {
                const newIndices =
                    indices.map(index => index + this.currentCapture - 1)
                this.groups.set(name, newIndices);
            }
        }

        this.currentCapture += other.currentCapture - 1;
    }
}

// ----- EXTRA CREDIT: named capture groups -----

// You'll need to modify a few parts of your EngExp implementation.
// First, change the signatures of beginCapture() and endCapture() to the following:
//
//     beginCapture(name?: string): EngExp
//     endCapture(name?: string): EngExp
//
// JavaScript regular expressions don't support named groups out of the box,
// so we'll need to extend them. Change the signature of asRegExp() to the following:
//
//     asRegExp(): NamedRegExp
//
// The interface for NamedRegExp is provided below (don't change it!) and depends on
// the new interface for NamedRegExpExecArray.

interface NamedRegExpExecArray extends RegExpExecArray {
    groups: Map<string, string | undefined>;
}

interface NamedRegExp extends RegExp {
    exec(s: string): NamedRegExpExecArray | null;
}

// Essentailly, a NamedRegExp is like a RegExp, but when you call its exec() method
// you get back an array with an extra field, groups, which is a hashmap from the
// names of capture groups to what those groups matched. You can access it like this:
//
//     let r = new EngExp().beginCapture("justf").then("f").endCapture().then("oo").asRegExp()
//     r.exec("foo").groups["justf"]  ==  "f"
//
// See the tests at the end of test/engexp.ts for more usage examples.
