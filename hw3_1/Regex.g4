grammar Regex;

re
    : alternative EOF
    ;

alternative
    : alternatives+=expression ('|' alternatives+=expression)+ # AlternativeGroup
    | expression # ExpressionAlternative
    ;

expression
    : (elems+=element)*
    ;

element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    ;

atom
    : c=CHARACTER # CharacterAtom
    | '(' subexpr=alternative ')' # SubexprAtom
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
