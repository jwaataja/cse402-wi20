const antlr4 = require("antlr4");
const RegexLexer = require("./gen/RegexLexer").RegexLexer;
const RegexParser = require("./gen/RegexParser").RegexParser;
const RegexListener = require("./gen/RegexListener").RegexListener;
const ErrorListener = require("antlr4/error/ErrorListener").ErrorListener;

// This enum gives us a unique object we can detect. You can extend it
// with more cases if your implementation needs it.
const Exprs = Object.freeze({
    EXP: Symbol("expression"),
    ALT: Symbol("alternative"),
});

// These names are just unique identifiers we'll use when creating the highlighting
// DOM structure. You shouldn't need to modify them.
const sourceName = "hoverSource";
const targetName = "hoverTarget";

// You shouldn't need to change any of the labeling or hoverScript code to get the
// compiler working. The rest of the code should make it clear how to use it.
function labeledSpan(text, label) {
    return "<span class=\"" + label + "\">" + text + "</span>";
}

// Used to escape error messages so that the user can't inject html into themselves.
// Credit for this function to: https://stackoverflow.com/a/30970751/1907543
// I've verified it is reasonable but did not write it.
function escapeHtml(s) {
    return s.replace(
        /[^0-9A-Za-z ]/g,
        c => "&#" + c.charCodeAt(0) + ";"
    );
}

// To collect error messages for displaying.
class CompilerErrorListener extends ErrorListener {
    constructor() {
        super();
        // this is a comprehensive list of error messages.
        this.errors = [];
    }

    // handle syntax errors and output them to the user!
    syntaxError(recognizer, offendingSymbol, line, column, msg, e) {
        this.errors.push(`line ${line}:${column} ${msg}`);
    }

    // produces HTML output to display the error list.
    finish() {
        return this.errors.map(escapeHtml).join('<br>');
    }
}

// To use the ANTLR4 JavaScript backend, we extend the listener class that has
// been generated for our grammar. By overriding certain methods of this class, we
// control what happens when the compiler walks over the parse tree.
class CompilerListener extends RegexListener {
    constructor() {
        super();
        // This compiler uses a stack to keep track of partial results from
        // walking over a node's children.
        this.stack = [];
        this.currentLabel = 1;
        this.script = "";
        this.result = "";
    }

    nextLabelPair() {
        const labelID = this.currentLabel++;
        return [sourceName + labelID, targetName + labelID];
    }

    // You shouldn't need to change this
    hoverScript(source, target, color) {
        this.script += (
            `$('.${source}').hover(function(){$('.${target}').css('background','${color}');},
                                   function(){$('.${target}').css('background','');});`
                + `$('.${source}').hover(function(){$('.${source}').css('background','cyan');},
                                         function(){$('.${source}').css('background','');});`
        ).replace(/\s/g, "");
    }

    // These "enter" and "exit" methods are where the comiling happens. They allow us
    // to react to events as we walk the parse tree. Usually we want to do things
    // in an "exit" handler, because at that point we've already walked over all of
    // the node's children and have the results available. The names of the handler
    // functions are based on the names of rules in the grammar.

    // Here, we use an "enter" handler to record the beginning of an expression.
    // As we walk over its children, we'll push partial results on top of this token.
    enterExpression(ctx) {
        this.stack.push(Exprs.EXP);
    }

    // Compiling an atom is easy: we just want to print it out as text. We can access
    // the token as ctx.c because we named it that way in the grammar. Lexer rule contexts
    // hold their text in the "text" field; for parser rules you can get the text of the
    // expression by calling the .getText() method.

    // We push the text onto the compiler stack so that parent rules can do things with it.
    exitCharacterAtom(ctx) {
        this.stack.push(ctx.c.text);
    }

    exitSubexprAtom(ctx) {
        const [source, target] = this.nextLabelPair();
        const subexpr = labeledSpan(this.stack.pop(), target);
        const left = labeledSpan('(', source);
        const right = labeledSpan(')', source);
        this.hoverScript(source, target, "orange");
        this.stack.push(left + subexpr + right);
    }

    // The element rule in the grammar has two cases. For the first case, we don't need to
    // do anything because of our handler for atoms (we would just push the same text back
    // on the stack). Since we've names the cases, we can write a handler specifically for
    // the second one.

    // If an atom has a modifier, we need to do something to highlight it. To do that we
    // get the text of the atom from the top of the stack, wrap it in appropriate labeled
    // spans, and push it back. We also need to register some code to do the highlighting.
    exitModifiedElement(ctx) {
        // this just makes two unique labels (as strings)
        const [source, target] = this.nextLabelPair();
        // get the text of the atom we want to decorate with a modifier
        const atom = this.stack.pop();
        // get the modifier token
        const modifier = ctx.m.text;

        // We label the atom as the target, and the modifier as the source. This means
        // that when we mouse over the source in the browser, we'll see highlighting
        // on the target (and cyan highlighting on the source).
        const htmlBody = labeledSpan(atom, target) + labeledSpan(modifier, source);

        // This just generates some JQuery to register the syntax highlighting effect.
        // The color is for the highlighting of the target; the source is always cyan.
        this.hoverScript(source, target, "blue");

        this.stack.push(htmlBody);
    }

    // Expressions are interesting because they can contain multiple elements.
    // Recall that when we enter an expression, we pushed a special token on the stack
    // to mark that point. Now that we're exiting the expression, we will already have
    // walked over all of our children (and pushed partial results for each onto the stack).
    // To get all the results, we just need to pop from the stack until we see that token.
    exitExpression(ctx) {
        let htmlBody = "";

        // expressions don't need to do any highlighting - just append the text together
        for (let expr = this.stack.pop(); expr !== Exprs.EXP; expr=this.stack.pop()) {
        // We will have walked the rightmost child last, so that will be on the top
        // of the stack. So we need to append each successive child onto the left side.
            htmlBody = expr + htmlBody;
        }

        this.stack.push(htmlBody);
    }

    enterAlternativeGroup(ctx) {
        this.stack.push(Exprs.ALT);
    }

    exitAlternativeGroup(ctx) {
        const [source, target] = this.nextLabelPair();
        let htmlBody = labeledSpan(this.stack.pop(), target);
        for (let expr = this.stack.pop(); expr !== Exprs.ALT; expr = this.stack.pop()) {
            // TODO: Change this to read the token itself?
            htmlBody = labeledSpan(expr, target) + labeledSpan('|', source) + htmlBody;
        }

        this.hoverScript(source, target, "yellow");
        this.stack.push(htmlBody);
    }

    // TODO: Exit handler for ExpressionAlternative? Doesn't seem necessary.

    // This handler fires when we finish the walk and exit the root "re" rule. All it
    // needs to do is provide the result from the single child.
    exitRe(ctx) {
        this.result = this.stack.pop();
    }
}

// This is the magic incantation to set up the compiler pipeline
// and run both the parser and the tree walk. We export the just the
// compile function (which takes a regexp string as input and returns
// the labeled html spans and the registration script which needs to be run).
// index.html knows what to do with this once you do "npm build" to run browserify.

function lexAndParse(input) {
    const chars = new antlr4.InputStream(input);
    const lexer = new RegexLexer(chars);
    const tokens = new antlr4.CommonTokenStream(lexer);
    const parser = new RegexParser(tokens);
    const errorListener = new CompilerErrorListener();
    lexer.addErrorListener(errorListener);
    parser.addErrorListener(errorListener);
    parser.buildParseTrees = true;
    const tree = parser.re();
    return [tree, errorListener.finish()];
}

function compile(input) {
    const [tree, errors] = lexAndParse(input);
    const compiler = new CompilerListener();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(compiler, tree);
    return [compiler.result + '<br>' + errors, compiler.script];
}

module.exports = compile;
