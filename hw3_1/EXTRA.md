# Extra Work

For this homework, we decided to investigate error handling in antlr4, and see
if we could produce meaningful error messages rather than simply ignoring
invalid input.

We imported the error listener class, ErrorListener, created a new error
listener which collectes all error messages, using the same error message
format which is output to console, and then produces a bit of HTML to display
this error list.

We then modified 'lexAndParse' to construct an error listener and register it
with both the lexer and parser, so as to collect all kinds of errors.
lexAndParse now returns a tuple of the tree and an HTML string description of
all errors which occurred.

Last, we modified compile to accept this error HTML output from lexAndParse and
include it in the final output!

To test out this feature, try including invalid characters, such as '[]', in the
regex.

    hello [] world!

outputs

    helloworld
    line 1:6 token recognition error at: '['
    line 1:7 token recognition error at: ']'
    line 1:14 token recognition error at: '!'

Or to test the parser, rather than the lexer, try using unmatched parenthesis:

    (This is invalid lisp.))

outputs

    (Thisisinvalidlisp.)
    line 1:23 extraneous input ')' expecting <EOF>

They aren't the prettiest errors, but they should be better than completely
ignoring invalid characters. Errors are correctly cleared after every compile.
