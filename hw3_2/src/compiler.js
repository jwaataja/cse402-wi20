const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

function eachHelper(_ctx, body, lst) {
    let res = "";
    for (const item of lst) {
        res += body(item);
    }
    return res;
}

function ifHelper(ctx, body, expr) {
    if (expr) {
        return body(ctx);
    } else {
        return "";
    }
}

function withHelper(ctx, body, expr) {
    return body(ctx[expr]);
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
        this._usedHelpers = new Map();
        // register standard helpers
        this.registerBlockHelper("each", eachHelper);
        this.registerBlockHelper("if", ifHelper);
        this.registerBlockHelper("with", withHelper);
    }

    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    compile(template) {
        this._usedHelpers.clear();
        this._bodyStack = [];
        this.pushScope();

        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        for (const [funcName, types] of this._usedHelpers) {
            for (const type of types) {
                const func = this._helpers[type][funcName];
                this._bodyStack[this._bodyStack.length - 1] = `const __$${funcName} = ${func.toString()};\n`
                    + this._bodyStack[this._bodyStack.length - 1];
            }
        }

        return this.popScope();
    }

    pushScope() {
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    popScope() {
        this._bodyStack[this._bodyStack.length - 1] += `return ${this._outputVar};\n`;
        return new Function(this._inputVar, this._bodyStack.pop());
    }

    append(expr) {
        this._bodyStack[this._bodyStack.length - 1] += `${this._outputVar} += ${expr};\n`;
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    exitRegularExpressionCase(ctx) {
        ctx.source = ctx.expr.source;
    }

    exitHelperExpressionCase(ctx) {
        ctx.source = ctx.expr.source;
    }

    exitDataLookup(ctx) {
        ctx.source = `${this._inputVar}.${ctx.id.text}`;
    }

    exitLiteralExpression(ctx) {
        ctx.source = ctx.getText();
    }

    exitHelperExpression(ctx) {
        const name = ctx.helperName.text;
        if (!this._usedHelpers.has(name)) {
            this._usedHelpers.set(name, new Set());
        }

        this._usedHelpers.get(name).add("expr");
        ctx.source = `__$${name}(${this._inputVar}`;
        for (const arg of ctx.helperArgs) {
            ctx.source += `, ${arg.source}`;
        }

        ctx.source += ')';
    }

    exitParenthesizedExpression(ctx) {
        ctx.source = ctx.expr.source;
    }

    exitExpressionElement(ctx) {
        this.append(ctx.expr.source);
    }

    enterBlockElement(ctx) {
        if (ctx.helperName.text !== ctx.endName.text) {
            throw new Error(`Block start '${ctx.helperName.text}' does not `
                            + `match the block end '${ctx.endName.text}'.`);
        }
        this.pushScope();
    }

    exitBlockElement(ctx) {
        const name = ctx.helperName.text;
        if (!this._usedHelpers.has(name)) {
            this._usedHelpers.set(name, new Set());
        }

        this._usedHelpers.get(name).add("block");
        const func = this.popScope();
        let result = `__$${name}(${this._inputVar}, ${func.toString()}`;
        for (const arg of ctx.helperArgs) {
            result += `, ${arg.source}`;
        }

        result += ')';
        this.append(result);
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;
