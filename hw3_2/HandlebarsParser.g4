parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

helperExpression returns [String source]
    : helperName=ID (helperArgs+=regularExpression)+
    ;

literal
    : INTEGER
    | FLOAT
    | STRING
    ;

regularExpression returns [String source]
    : id=ID # DataLookup
    | OPEN_PAREN expr=expression CLOSE_PAREN # ParenthesizedExpression
    | literal # LiteralExpression
    ;

expression returns [String source]
    : expr=regularExpression # RegularExpressionCase
    | expr=helperExpression # HelperExpressionCase
    ;

expressionElement returns [String source]
    : START expr=expression END
    ;

blockElement
    : START BLOCK helperName=ID helperArgs+=regularExpression* END element* START CLOSE_BLOCK endName=ID END
    ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement
    : TEXT
    | TEXT? (BRACE TEXT)+;

commentElement : START COMMENT END_COMMENT ;
