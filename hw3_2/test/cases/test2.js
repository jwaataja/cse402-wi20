exports.helpers = [
    ["list", (ctx, ...items) => items],
    ["ctx", (ctx) => ctx],
    ["first", (ctx, lst) => lst[0]],
    ["last", (ctx, lst) => lst[lst.length - 1]],
];
exports.blockHelpers = [];
exports.ctx = {
    this_string: "this",
};
exports.description = "Intricate expr helper usage";
