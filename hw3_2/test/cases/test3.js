exports.helpers = [
    ["list", (ctx, ...items) => items],
    ["ctx", (ctx) => ctx],
    ["first", (ctx, lst) => lst[0]],
    ["last", (ctx, lst) => lst[lst.length - 1]],
];
exports.blockHelpers = [];
exports.ctx = {};
exports.description = "Parsing and expr edge cases";
