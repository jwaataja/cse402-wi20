const expect = require("chai").expect;
const Stream = require("../src/rx").Stream;

describe("Stream.latest", () => {
    it("passes through events from one stream", () => {
        const result = [];
        const first = new Stream();
        const out = first.latest();
        out.subscribe(x => result.push(x));

        let innerStream = new Stream();
        first._push(innerStream);
        innerStream._push(42);
        innerStream._push(57);
        innerStream._push(61);
        expect(result).to.deep.equal([42, 57, 61]);
    });
    it("ignores streams which haven't yet produced value", () => {
        const result = [];
        const first = new Stream();
        const out = first.latest();
        out.subscribe(x => result.push(x));

        let innerStream = new Stream();
        first._push(innerStream);
        first._push(new Stream());
        first._push(new Stream());
        first._push(new Stream());
        innerStream._push(42);
        innerStream._push(57);
        innerStream._push(61);
        expect(result).to.deep.equal([42, 57, 61]);
    });
    it("switches streams", () => {
        const result = [];
        const first = new Stream();
        const out = first.latest();
        out.subscribe(x => result.push(x));

        let stream1 = new Stream();
        let stream2 = new Stream();
        first._push(stream1);
        stream1._push(42);
        first._push(stream2);
        stream2._push(61);
        expect(result).to.deep.equal([42, 61]);
    });
    it("ignores old streams once it has the new one", () => {
        const result = [];
        const first = new Stream();
        const out = first.latest();
        out.subscribe(x => result.push(x));

        let stream1 = new Stream();
        let stream2 = new Stream();
        first._push(stream1);
        stream1._push(42);
        first._push(stream2);
        stream2._push(61);
        stream1._push("bad");
        expect(result).to.deep.equal([42, 61]);
    });
    it("picks up intermediate streams when given many", () => {
        const result = [];
        const first = new Stream();
        const out = first.latest();
        out.subscribe(x => result.push(x));

        let stream1 = new Stream();
        let stream2 = new Stream();
        let stream3 = new Stream();
        let stream4 = new Stream();
        first._push(stream1);
        first._push(stream2);
        first._push(stream3);
        first._push(stream4);
        stream1._push(42);
        stream2._push(43);
        stream3._push(44);
        stream4._push(45);
        expect(result).to.deep.equal([42, 43, 44, 45]);
    });
    it("ignores old streams when given many", () => {
        const result = [];
        const first = new Stream();
        const out = first.latest();
        out.subscribe(x => result.push(x));

        let stream1 = new Stream();
        let stream2 = new Stream();
        let stream3 = new Stream();
        let stream4 = new Stream();
        first._push(stream1);
        first._push(stream2);
        first._push(stream3);
        first._push(stream4);
        stream1._push(42);
        stream3._push(44);
        // stream2 is now actually old
        stream2._push(43);
        stream4._push(45);
        expect(result).to.deep.equal([42, 44, 45]);
    });
});

describe("Stream.unique", () => {
    it("passes through events", () => {
        const result = [];
        const first = new Stream();
        const out = first.unique(x => x);
        out.subscribe(x => result.push(x));

        first._push(42);
        expect(result).to.deep.equal([42]);
    });
    it("stops immediate duplicates", () => {
        const result = [];
        const first = new Stream();
        const out = first.unique(x => x);
        out.subscribe(x => result.push(x));

        first._push(42);
        first._push(42);
        expect(result).to.deep.equal([42]);
    });
    it("stops historical duplicates", () => {
        const result = [];
        const first = new Stream();
        const out = first.unique(x => x);
        out.subscribe(x => result.push(x));

        first._push(42);
        first._push(5);
        first._push(42);
        expect(result).to.deep.equal([42, 5]);
    });
    it("stops unique values with same hash", () => {
        const result = [];
        const first = new Stream();
        const out = first.unique(obj => obj['a']);
        out.subscribe(x => result.push(x));

        first._push({a: 42, b: 3});
        first._push({a: 42, b: 5});
        expect(result).to.deep.equal([{a: 42, b: 3}]);
    });
});
