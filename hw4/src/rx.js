class Stream {
    constructor() {
        this.callbacks = [];
    }

    // Add Stream methods here.
    subscribe(listener) {
        this.callbacks.push(listener);
    }

    _push(item) {
        for (const callback of this.callbacks) {
            callback(item);
        }
    }

    _push_many(items) {
        for (const item of items) {
            this._push(item);
        }
    }

    first() {
        const result = new Stream();
        let pushed = false;
        this.subscribe(item => {
            if (!pushed) {
                result._push(item);
                pushed = true;
            }
        });
        return result;
    }

    map(func) {
        const result = new Stream();
        this.subscribe(item => result._push(func(item)));
        return result;
    }

    filter(predicate) {
        const result = new Stream();
        this.subscribe(item => {
            if (predicate(item)) {
                result._push(item);
            }
        });
        return result;
    }

    distinctUntilChanged() {
        const result = new Stream();
        let hasLast = false;
        let last = null;
        this.subscribe(item => {
            // Not using "!=" here in case of javascript weirdness.
            if (!hasLast || !(item == last)) {
                result._push(item);
            }

            hasLast = true;
            last = item;
        });
        return result;
    }

    unique(hashFunc) {
        const result = new Stream();
        const seen = new Set();
        this.subscribe(item => {
            const hash = hashFunc(item);
            if (!seen.has(hash)) {
                seen.add(hash);
                result._push(item);
            }
        });
        return result;
    }

    flatten() {
        const result = new Stream();
        this.subscribe(item => {
            if (Array.isArray(item)) {
                result._push_many(item);
            } else {
                result._push(item);
            }
        });
        return result;
    }

    scan(func, acc) {
        const result = new Stream();
        let current = acc;
        this.subscribe(item => {
            const next = func(current, item);
            result._push(next);
            current = next;
        });
        return result;
    }

    join(other) {
        const result = new Stream();
        this.subscribe(item => result._push(item));
        other.subscribe(item => result._push(item));
        return result;
    }

    combine() {
        const result = new Stream();
        this.subscribe(stream => {
            stream.subscribe(item => result._push(item));
        });
        return result;
    }

    zip(other, func) {
        const result = new Stream();
        let hasLast = false;
        let last = null;
        let hasOtherLast = false;
        let otherLast = null;
        this.subscribe(item => {
            hasLast = true;
            last = item;
            if (hasOtherLast) {
                result._push(func(last, otherLast));
            }
        });
        other.subscribe(item => {
            hasOtherLast = true;
            otherLast = item;
            if (hasLast) {
                result._push(func(last, otherLast));
            }
        });
        return result;
    }

    throttle(milliseconds) {
        const result = new Stream();
        let cancelLast = null;
        this.subscribe(item => {
            if (cancelLast != null) {
                cancelLast();
            }

            let canceled = false;
            cancelLast = () => canceled = true;
            const callback = () => {
                if (!canceled) {
                    result._push(item);
                    cancelLast = null;
                }
            };
            setTimeout(callback, milliseconds);
        });
        return result;
    }

    latest() {
        const result = new Stream();
        let cancelPrev = null;
        this.subscribe(stream => {
            // the cancel function for all previously visited streams
            // note: intentionally set to null after use so that we
            // don't accidentally keep a bunch of old functions alive
            let currentCancel = cancelPrev;
            let shouldPush = true;
            cancelPrev = () => {
                shouldPush = false;
                if (currentCancel != null) {
                    currentCancel();
                    currentCancel = null;
                }
            };
            stream.subscribe(item => {
                // recursively cancel all older subscriptions
                if (currentCancel != null) {
                    currentCancel();
                    currentCancel = null;
                }

                if (shouldPush) {
                    result._push(item);
                }
            });
        });

        return result;
    }

    static timer(milliseconds) {
        const result = new Stream();
        setInterval(() => result._push((new Date()).getTime()), milliseconds);
        return result;
    }

    static dom(element, eventName) {
        const result = new Stream();
        element.on(eventName, ev => result._push(ev));
        return result;
    }

    static url(requestUrl) {
        const result = new Stream();
        const callback = resultJson => result._push(resultJson);
        $.get(requestUrl, callback, "json");
        return result;
    }

    static ajax(_url, _data) {
        var out = new Stream();
        $.ajax({
            url: _url,
            jsonp: "callback",
            dataType: "jsonp",
            data: _data,
            success: function(data) { out._push(data[1]); }
        });
        return out;
    }
}

if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    function WIKIPEDIAGET(s) {
        return Stream.ajax("https://en.wikipedia.org/w/api.php?format=json", { action: "opensearch", search: s, namespace: 0, limit: 10 });
    }

    // Add your hooks to implement Parts 2-4 here.
    const timer = Stream.timer(2000);
    timer.subscribe(time => $("#time").text(time));

    let clickCount = 0;
    const clickStream = Stream.dom($("#button"), "click");
    clickStream.subscribe(ev => {
        clickCount++;
        $("#clicks").text(clickCount.toString());
    });

    const mouseStream = Stream.dom($("#mousemove"), "mousemove")
        .throttle(1000);
    mouseStream.subscribe(ev => {
        $("#mouseposition").text(ev.pageX + ", " + ev.pageY);
    });

    const fireUpdateStream = Stream.timer(60 * 1000)
        .map(() => Stream.url(FIRE911URL))
        .latest()
        .map(data => {
            const sorted = [...data];
            sorted.sort((a, b) => a["updated_at"] - b["updated_at"]);
            return sorted;
        })
        .flatten()
        .unique(item => item["id"])
        .map(item => item["405818852"]);

    let knownFireEvents = [];
    let fireSearchTerm = null;
    fireUpdateStream.subscribe(desc => knownFireEvents.push(desc));

    const fireSearchStream = Stream.dom($("#firesearch"), "input")
        .distinctUntilChanged()
        .map(evt => evt.target.value);

    // update UI with new events
    fireUpdateStream.subscribe(desc => {
        if (fireSearchTerm == null || desc.includes(fireSearchTerm)) {
            $("#fireevents").append($("<li></li>").text(desc));
        }
    });

    // update UI with new search term
    fireSearchStream.subscribe(term => {
        if (term == "") {
            fireSearchTerm = null;
        } else {
            fireSearchTerm = term;
        }
        $("#fireevents").empty();
        for (const desc of knownFireEvents) {
            if (fireSearchTerm == null || desc.includes(fireSearchTerm)) {
                $("#fireevents").append($("<li></li>").text(desc));
            }
        }
    });

    const searchEventStream = Stream.dom($("#wikipediasearch"), "keyup")
        .map(ev => ev.target.value)
        .throttle(100)
        .map(WIKIPEDIAGET)
        .latest()
        .filter(item => item !== undefined);
    searchEventStream.subscribe(suggestions => {
        $("#wikipediasuggestions").empty();
        for (const suggestion of suggestions) {
            $("#wikipediasuggestions").append($("<li></li>").text(suggestion));
        }
    });

    loadingBar($("#load-more"), $("#load-complete"), $("#progress"));
}

function loadingBar(loadMore, loadComplete, progressBar) {
    let assumedTotal = 10;
    let processed = 0;
    let loadStream = Stream.dom(loadMore, "click")
        .map(_e => "loadMore");
    let completeStream = Stream.dom(loadComplete, "click")
        .map(_e => "loadComplete");

    let merged = loadStream.join(completeStream);

    merged.subscribe(ty => {
        if (ty == "loadComplete") {
            processed = assumedTotal;
        } else {
            processed += 1;
            if (processed > assumedTotal) {
                assumedTotal *= 2;
            }
        }
        let percent = 100 * processed / assumedTotal;
        progressBar.width(percent + '%');
        // set class to progress.finished iff processed == assumedTotal
        progressBar.toggleClass("finished", processed == assumedTotal);
    })
}
