import {expect} from "chai";

import * as q from "../src/q";
import * as t from "./q";
const Q = q.Q;

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

// a relatively diverse but still simple data set we can execute numerous queries on
const testData = [
    {ty: "l", la: 0, b: 1},
    {ty: "l", la: 10, b: 1},
    {ty: "l", la: 20, b: -5},
    {ty: "l", la: 40, b: 30},
    {ty: "r", ra: 10, c: 1},
    {ty: "r", ra: 5, c: 2},
    {ty: "r", ra: 10, c: 3},
    {ty: "r", ra: 5, c: 4},
]

describe("individual nodes", () => {
    it("should function independently (filter)", () => {
        const query = Q.filter(x => x.ty === "l");
        const expected = [
            {ty: "l", la: 0, b: 1},
            {ty: "l", la: 10, b: 1},
            {ty: "l", la: 20, b: -5},
            {ty: "l", la: 40, b: 30},
        ];
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
    it("should function independently (apply)", () => {
        const query = Q.apply(({ty, ...rest}) => ({the_type: ty, ...rest}));
        const expected = [
            {the_type: "l", la: 0, b: 1},
            {the_type: "l", la: 10, b: 1},
            {the_type: "l", la: 20, b: -5},
            {the_type: "l", la: 40, b: 30},
            {the_type: "r", ra: 10, c: 1},
            {the_type: "r", ra: 5, c: 2},
            {the_type: "r", ra: 10, c: 3},
            {the_type: "r", ra: 5, c: 4},
        ];
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
    it("should function independently (count)", () => {
        const query = Q.count();
        const expected = [8];
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
    it("should function independently (product)", () => {
        const query = Q.product(Q);
        const expected_1 = {left: {ty: "l", la: 0, b: 1}, right: {ty: "r", ra: 10, c: 1}};
        const expected_2 = {left: {ty: "l", la: 10, b: 1}, right: {ty: "r", ra: 5, c: 4}};
        expect(query.execute(testData)).to.deep.include(expected_1)
            .and.to.deep.include(expected_2);
        expect(query.optimize().execute(testData)).to.deep.include(expected_1)
            .and.to.deep.include(expected_2);
    });
    it("should function independently (join)", () => {
        const query = Q.join(Q, (l, r) => l.ty === "l" && r.ty === "r" && l.la === r.ra);
        const expected = [
            {ty: "r", la: 10, b: 1, ra: 10, c: 1},
            {ty: "r", la: 10, b: 1, ra: 10, c: 3},
        ];
        expect(query.execute(testData)).to.deep.equal(expected)
        expect(query.optimize().execute(testData)).to.deep.equal(expected)
    });
});

describe("a raw chain", () => {
    it("of filter and count should function", () => {
        const query = Q.filter(x => x.ty === "r").count();
        const expected = [4];
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
    it("of filter+filter, product, filter and apply should function", () => {
        // ensure our optimizations don't conflict with manual construction which happens to match the pattern of 'join'
        const leftQuery = Q.filter(x => x.ty == "l");
        const rightQuery = Q.filter(x => x.ty == "r");
        const query = leftQuery.product(rightQuery).filter(x => x.left.la === x.right.ra).apply(({left, right}) => ({b: left.b, c: right.c}));
        const expected = [
            {b: 1, c: 1},
            {b: 1, c: 3},
        ];
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
    // more optimization testing
    it("of filter, filter and count should function", () => {
        const query = Q.filter(x => x.ty === "r").filter(x => x.ra === 10 || x.la !== undefined).count();
        const expected = [2];
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
    it("of nested counts should function", () => {
        const query = Q.count().count().count().count();
        const expected = [1];
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
});

describe("a weird manual combination", () => {
    it("of tons of then and id nodes should still be a no-op", () => {
        const query = new q.ThenNode(
            new q.ThenNode(
                new q.IdNode(),
                new q.ThenNode(new q.IdNode(), new q.IdNode())
            ),
            new q.ThenNode(
                new q.ThenNode(new q.IdNode(), new q.ThenNode(new q.IdNode(), new q.IdNode())),
                new q.IdNode()
            )
        );
        expect(query.execute(testData)).to.deep.equal(testData);
        expect(query.optimize().execute(testData)).to.deep.equal(testData);
    });
    it("of filters on the wrong side of then nodes should produce a correct result", () => {
        const query = new q.ThenNode(
            new q.FilterNode(x => x.ty === "l"),
            new q.ThenNode(
                new q.FilterNode(x => x.la <= 20),
                new q.FilterNode(x => x.b > 0),
            ),
        );
        const expected = [
            {ty: "l", la: 0, b: 1},
            {ty: "l", la: 10, b: 1},
        ]
        expect(query.execute(testData)).to.deep.equal(expected);
        expect(query.optimize().execute(testData)).to.deep.equal(expected);
    });
});

function flattenThenNodes(query) {
    if (query instanceof q.ThenNode) {
        return flattenThenNodes(query.first).concat(flattenThenNodes(query.second));
    } else if (query instanceof q.IdNode) {
        return [];
    } else {
        return [query];
    }
}

function expectQuerySequence(query, seq) {
    const flat = flattenThenNodes(query);
    expect(flat.length).to.be.equal(seq.length);
    for (let i = 0; i < flat.length; i++) {
        expect(flat[i].type).to.be.equal(seq[i]);
    }
}

describe("Extra credit tests", () => {
    it("should sort correctly", () => {
        const data = [3, 2, 1];
        const expected = [1, 2, 3];
        const query = Q.sort((x, y) => x - y);
        const out = query.execute(data);
        expect(out).to.deep.equal(expected);
    });

    it("should have grouping", () => {
        const data = [
            { a: 1, b: 1},
            { a: 1, b: 2},
            { a: 2, b: 3},
            { a: 2, b: 4}
        ];
        const expected = [
            [1, [{ a: 1, b: 1}, { a: 1, b: 2 }]],
            [2, [{ a: 2, b: 3}, { a: 2, b: 4 }]]
        ];
        const query = Q.group_by(x => x.a);
        const out = query.execute(data);
        expect(out).to.deep.equal(expected);
    });

    it("should have reduce", () => {
        const data = [1, 2, 3];
        const query = Q.reduce((a, b) => a + b, 1);
        const out = query.execute(data);
        expect(out).to.deep.equal([7]);
    });
});

describe("Extra credit optimizations", () => {
    it("should fuse apply nodes", () => {
        const query = Q.apply(x => x).apply(x => x).apply(x => x);
        expectQuerySequence(query.optimize(), ["Apply"]);
    });

    it("should remove apply nodes directly before count nodes", () => {
        const query = Q.apply(x => x).count();
        expectQuerySequence(query.optimize(), ["Count"]);
    });
});
