# Extra Credit

I did suggestions 1-3.

For suggestion 2, I wasn't exactly sure what the result of a query should look like with grouping,
so I made it return an array for each group where the first element is the value of the key for
that group and the second element is an array of the records in that group. There's a test showing
how this works.

I did an optimization to fuse apply nodes.

I also did an optimization to remove apply nodes before a count node. This doesn't change output
because an apply never changes the number of records if there's no filter in between.
