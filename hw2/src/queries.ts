import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.FilterNode(
    x => x[2].startsWith("THEFT") || x[2].startsWith("VEH-THEFT"));
export const autoTheftsQuery = new q.FilterNode(x => x[2].startsWith("VEH-THEFT"));

//// 1.4 clean the data

export const cleanupQuery = new q.ApplyNode(
    x => ({ description: x[2], category: x[3], area: x[4], date: x[5] }));

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply(
    x => ({ description: x[2], category: x[3], area: x[4], date: x[5] }));
export const theftsQuery2 = Q.filter(
    x => x.description.startsWith("THEFT")
        || x.description.startsWith("VEH-THEFT"));
export const autoTheftsQuery2 = Q.filter(
    x => x.description.startsWith("VEH-THEFT"));;

//// 4 put your queries here (remember to export them for use in tests)

// Note: These queries run on the cleaned data like in 1.6

// This query finds all pairs of different types of theft that occurred in the
// same year. To prevent some duplicates, it returns the pairs with the first
// type of theft lexicographically before the second. Precisely, it returns each
// pair of thefts that occurred in the same year where the first type of theft
// is distinct from the second and occurs first lexicographically. It works by
// creating two queries that filter for theft and then each uses an apply to
// reduce the number of fields and rename them. They're joined on having the
// same year and when the first's description is lexicographically before the
// second.
export const theftYearQuery = Q.filter(x => x.description.startsWith("THEFT"))
    .apply(x => ({
        description1: x.description,
        date1: x.date
    }))
    .join(Q.filter(x => x.description.startsWith("THEFT"))
            .apply(x => ({
                description2: x.description,
                date2: x.date
             })),
          (x, y) => x.date1.substring(0, 4) === y.date2.substring(0, 4)
              && x.description1 < y.description2);

// This query returns all the types of theft. Precisely, for each crime with
// description "THEFT-" it returns the rest of the description, that is, the
// type of theft. It does this by filtering for thefts and then taking
              // substrings with an apply.
export const theftTypeQuery = Q.filter(x => x.description.startsWith("THEFT-"))
    .apply(x => ({ type: x.description.substring("THEFT-".length) }));

// This query returns the subcategories of robbery. It first filters based on
// robberies and has an apply that returns only the subcategory.
export const robberyQuery = Q.filter(x => x.description.startsWith("ROBBERY"))
    .apply(x => ({ category: x.category }));
