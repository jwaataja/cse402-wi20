/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

interface FieldJoinPredicate {
    (datum: any): boolean;
    field: string;
}

function isFieldJoinPredicate(pred: any): pred is FieldJoinPredicate {
    return "field" in pred && typeof pred.field === "string";
}

interface JoinApplyFunc {
    (datum: any): any;
    isJoinApplyFunc: boolean;
}

function isJoinApplyFunc(func: any): func is JoinApplyFunc {
    return "isJoinApplyFunc" in func
        && typeof func.isJoinApplyFunc === "boolean"
        && func.isJoinApplyFunc;
}

// This class represents all AST Nodes.
export class ASTNode {
    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(callback: (datum: any) => any): ASTNode {
        return new ThenNode(this, new ApplyNode(callback));
    }

    count(): ASTNode {
        return new ThenNode(this, new CountNode());
    }

    product(query: ASTNode): ASTNode {
        return new CartesianProductNode(this, query);
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        const applyFunc = function(datum) {
            const result = new Object();
            Object.assign(result, datum.left);
            Object.assign(result, datum.right);
            return result;
        } as JoinApplyFunc;
        applyFunc.isJoinApplyFunc = true;

        if (typeof(relation) === "string") {
            const fieldName = relation as string;
            const pred = function(x) {
                return x.left[fieldName] === x.right[fieldName];
            } as FieldJoinPredicate;
            pred.field = fieldName;
            return this.product(query).filter(pred).apply(applyFunc);
        } else {
            return this.product(query)
                .filter(x => relation(x.left, x.right))
                .apply(applyFunc);
        }
    }

    sort(comparator: (left: any, right: any) => number): ASTNode {
        return new SortByNode(this, comparator);
    }

    group_by(key: (any) => any): ASTNode {
        return new GroupByNode(key, this);
    }

    reduce(func: (left: any, right: any) => any, initialRow: any): ASTNode {
        return new ReduceNode(this, func, initialRow);
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    public execute(data: any[]): any[] {
        return data;
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    public execute(data: any[]): any[] {
        return data.filter(this.predicate);
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    public execute(data: any[]): any[] {
        return this.second.execute(this.first.execute(data));
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    callback: (datum: any) => any;

    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.callback = callback;
    }

    public execute(data: any[]): any[] {
        return data.map(this.callback);
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    public execute(data: any[]): number[] {
        return [data.length];
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
/* AddOptimization(ThenNode, function(this: ThenNode): ASTNode { */
/*     if (this.first instanceof IdNode) { */
/*         return this.second; */
/*     } else if (this.second instanceof IdNode) { */
/*         return this.first; */
/*     } else { */
/*         return null; */
/*     } */
/* }); */

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof ThenNode
        && this.first.second instanceof FilterNode
        && this.second instanceof FilterNode) {
        let first = this.first.second as FilterNode;
        let second = this.second as FilterNode;
        if (first != null && second != null) {
            return new ThenNode(
                this.first.first,
                new FilterNode(x => (first.predicate(x) && second.predicate(x)))
            );
        }
    }

    if (this.first instanceof FilterNode && this.second instanceof FilterNode) {
        let first = this.first as FilterNode;
        let second = this.second as FilterNode;
        return new FilterNode(x => (first.predicate(x) && second.predicate(x)));
    }

    return null;
});

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof ThenNode
        && this.first.second instanceof FilterNode
        && this.second instanceof CountNode) {
        let filter = this.first.second as FilterNode;
        let count = this.second as CountNode;
        return new ThenNode(
            this.first.first,
            new CountIfNode(filter.predicate)
        );
    }

    if (this.first instanceof FilterNode && this.second instanceof CountNode) {
        let filter = this.first as FilterNode;
        return new CountIfNode(filter.predicate);
    }
    return null;
});

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof ApplyNode
        && this.first instanceof ThenNode
        && this.first.second instanceof FilterNode
        && this.first.first instanceof CartesianProductNode) {
        const apply = this.second as ApplyNode;
        const filter = this.first.second as FilterNode;
        const product = this.first.first as CartesianProductNode;

        if (isFieldJoinPredicate(filter.predicate)
            && isJoinApplyFunc(apply.callback)) {
            return new HashJoinNode(filter.predicate.field,
                                    product.left,
                                    product.right);
        }

        return new JoinNode(product.left,
                            product.right,
                            filter.predicate,
                            apply.callback);
    }

    return null;
});

// This extra credit optimization fuses adjacent apply nodes.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    /* console.log("in the thing"); */
    /* console.log(this); */
    if (this.second instanceof ApplyNode
        && this.first instanceof ThenNode
        && this.first.second instanceof ApplyNode) {
        const first = this.first.second as ApplyNode;
        const second = this.second as ApplyNode;
        return new ThenNode(this.first.first,
                            new ApplyNode(
                                x => second.callback(first.callback(x))));
    }

    if (this.second instanceof ApplyNode
        && this.first instanceof ApplyNode) {
        const first = this.first as ApplyNode;
        const second = this.second as ApplyNode;
        return new ApplyNode(x => second.callback(first.callback(x)));
    }

    return null;
});

// This extra credit optimization removes apply nodes that occur before a count
// node.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof CountNode
        && this.first instanceof ThenNode
        && this.first.second instanceof ApplyNode) {
        return new ThenNode(this.first.first, this.second);
    }

    if (this.second instanceof CountNode
        && this.first instanceof ApplyNode) {
        return new ThenNode(this.first, this.second);
    }

    return null;
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...


//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    public execute(data: any[]): number[] {
        let count = 0;
        for (const item of data) {
            if (this.predicate(item)) {
                count++;
            }
        }
        return [count];
    }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    public execute(data: any[]): object[] {
        const leftResult = this.left.execute(data);
        const rightResult = this.right.execute(data);
        let result = [];
        for (const left of leftResult) {
            for (const right of rightResult) {
                result.push({left: left, right: right});
            }
        }

        return result;
    }

    public optimize(): ASTNode {
        return new CartesianProductNode(this.left.optimize(),
                                        this.right.optimize());
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    predicate: (datum: any) => any;
    transformer: (datum: any) => any;

    constructor(left: ASTNode,
                right: ASTNode,
                predicate: (datum: any) => any,
                transformer: (datum: any) => any) { // you may want to add some proper arguments to this constructor
        super("Join");
        this.left = left;
        this.right = right;
        this.predicate = predicate;
        this.transformer = transformer;
    }

    public execute(data: any[]): any[] {
        const leftResult = this.left.execute(data);
        const rightResult = this.right.execute(data);
        let result = [];
        for (const left of leftResult) {
            for (const right of rightResult) {
                const pair = { left: left, right: right};
                if (this.predicate(pair)) {
                    result.push(this.transformer(pair));
                }
            }
        }

        return result;
    }

    public optimize(): ASTNode {
        return new JoinNode(this.left.optimize(),
                            this.right.optimize(),
                            this.predicate,
                            this.transformer);
    }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    field: string;

    constructor(field: string, left: ASTNode, right: ASTNode) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        this.left = left;
        this.right = right;
        this.field = field;
    }

    public execute(data: any[]): any[] {
        const left = this.left.execute(data);
        const right = this.right.execute(data);

        const leftHash = HashJoinNode.makeHash(left, this.field);
        const rightHash = HashJoinNode.makeHash(right, this.field);

        const result = new Array();
        for (const [field, leftRecords] of leftHash.entries()) {
            const rightRecords = rightHash.has(field)
                ? rightHash.get(field)
                : new Array();
            for (const leftRecord of leftRecords) {
                for (const rightRecord of rightRecords) {
                    const record = new Object();
                    Object.assign(record, leftRecord);
                    Object.assign(record, rightRecord);
                    result.push(record);
                }
            }
        }

        return result;
    }

    static makeHash(data: any[], field: string) {
        const result = new Map();
        for (const record of data) {
            const fieldVal = record[field];
            if (result.has(fieldVal)) {
                result.get(fieldVal).push(record);
            } else {
                result.set(fieldVal, [record]);
            }
        }

        return result;
    }

    public optimize(): ASTNode {
        return new HashJoinNode(this.field,
                                this.left.optimize(),
                                this.right.optimize());
    }
}

export class SortByNode extends ASTNode {
    query: ASTNode;
    comparator: (left: any, right: any) => number;

    constructor(query: ASTNode,
                comparator: (left: any, right: any) => number) {
        super("SortBy");
        this.query = query;
        this.comparator = comparator;
    }

    public execute(data: any[]): any[] {
        const result = this.query.execute(data);
        result.sort(this.comparator);
        return result;
    }

    public optimize(): ASTNode {
        return new SortByNode(this.query.optimize(), this.comparator);
    }
}

// I wasn't sure exactly what the form of the data execute should return, so I
// made it return records where the first entry is a particular key value and
// the second is an array of records with that key value. See tests for expected
// behavior.
export class GroupByNode extends ASTNode {
    query: ASTNode;
    key: (any) => any;

    constructor(key: (any) => any, query: ASTNode) {
        super("GroupBy");
        this.query = query;
        this.key = key;
    }

    public execute(data: any[]): any[] {
        const records = this.query.execute(data);
        const groups = new Map();
        for (const record of records) {
            const key = this.key(record);
            if (groups.has(key)) {
                groups.get(key).push(record);
            } else {
                groups.set(key, [record]);
            }
        }

        return Array.from(groups.entries());
    }

    public optimize(): ASTNode {
        return new GroupByNode(this.key, this.query.optimize());
    }
}

export class ReduceNode extends ASTNode {
    query: ASTNode;
    func: (left: any, right: any) => any;
    initialRow: any;

    constructor(query: ASTNode,
               func: (left: any, right: any) => any,
               initialRow: any) {
        super("Reduce");
        this.query = query;
        this.func = func;
        this.initialRow = initialRow;
    }

    public execute(data: any[]): any[] {
        return [this.query.execute(data).reduce(this.func, this.initialRow)];
    }

    public optimize(): ASTNode {
        return new ReduceNode(this.query.optimize(),
                              this.func,
                              this.initialRow);
    }
}
